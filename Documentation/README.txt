Part B: Design of Relational Schema


ToDo: [Insert XML Tree diagram here!]


=========================================================== Iteration 1 ================================================================

The initial step is to convert the Document Type Definition of the XML structure, Fig 1, into a database schema by loosely completing the following procedure starting from the root:

1 If a parent element node found, i.e. has children elements or attributes
	1.1 Name the table after the parent element
	1.2 Add a column in the current table corresponding to each child element and attributes of the parent or child
2 Move down to a child element that is also a parent and repeat start from 1

The result was the following tables:

TABLE Items (
	ItemID,
	Name,
	Category,
	Currently,
	Buy_Price,
	First_Bid,
	Number_of_Bids,
	BidderID		REFERENCES Bidders(BidderID),	[=UserID, attribute of Bidder][Field used temporary for easy to follow explanations]
	Time			REFERENCES Bids(Time),			[an element of Bid]
	Location,
	Latitude, 										[attribute of Location]
	Longitude, 										[attribute of Location]
	Country,
	Started,
	Ends,
	SellerID,										[=UserID, attribute of Seller]
	Rating,											[attribute of Seller]
	Description,

	PRIMARY KEY (ItemID)
)

However, 'ItemID' fails to be a primary key as the field is duplicated when combined with 'Category', or 'BidderID' and 'Time'

FD: {ItemID, BidderID, Time} -> {Number_of_Bids} 	// Violates BCNF => Split 'ItemID', and 'BidderID', 'Time' into different tables
FD: {Latitude, Longitude} -> {Location} 			// Violates 3NF  => Separate 'Latitude', 'Longitude' and 'Location' into their own table
FD: {SellerID} -> {Rating}							// Violates 3NF	 => Separate 'SellerID' and "Rating" into their own table

MVD: {ItemID} -->> {Category}						// Violates 4NF	 => Separate 'ItemID' and 'Category' into tehir own table
MVD: {ItemID} -->> {BidderID, Time}					// Violates 4NF	 => Separate 'ItemID' and 'BidderID', 'Time' into their own table

TABLE Bids (
	BidderID		REFERENCES Bidders(BidderID),	[=UserID, attribute of Bidder][Field used temporary for easy to follow explanations]
	Time,
	Amount,

	PRIMARY KEY (BidderID, Time)
)

TABLE Bidders (
	UserID,
	Rating,
	Location,
	Country,

	PRIMARY KEY (UserID)
)

Reflections of the above reasoning and resulting normalisation are shown in Iteration 2

=========================================================== Iteration 2 ================================================================

TABLE Items (
	ItemID,
	Name,
	Currently,
	Buy_Price,
	First_Bid,
	Number_of_Bids,
	Location 		REFERENCES Locations(Location),
	Country,
	Started,
	Ends,
	SellerID 		REFERENCES Sellers(UserID),		[=UserID, attribute of Seller]
	Description,

	PRIMARY KEY (ItemID)
)

TABLE Item_Categories (
	ItemID			REFERENCES Items(ItemID),
	Category,

	PRIMARY KEY (ItemID, Category)
)

TABLE Item_Bids (
	ItemID			REFERENCES Items(ItemID),
	BidderID 		REFERENCES Bidders(BidderID),
	Time 			REFERENCES Bids(Time),

	PRIMARY KEY (ItemID, BidderID, Time)
)

TABLE Locations (
	Location,
	Latitude,
	Longitude,

	PRIMARY KEY (Latitude, Longitude)
)

TABLE Sellers (
	UserID,
	Rating,

	PRIMARY KEY (UserID)
)

TABLE Bids (
	BidderID		REFERENCES Bidders(BidderID),	[=UserID, attribute of Bidder][Field used temporary for easy to follow explanations]
	Time,
	Amount,

	PRIMARY KEY (BidderID, Time)
)

TABLE Bidders (
	UserID,
	Rating,
	Location,
	Country,

	PRIMARY KEY (UserID)
)

However, the fields 'Buy_Price' of Items, 'Location' and 'Country' of Bidders are optional. This means that if the current schema is preserved in its current form, there may be empty/null values corresponding to records lacking those elements. This is bad practice and, hence, these fields are normalised into tables of their own together with the keys of their current tables:

 - TABLE (ItemID, Buy_Price)
 - TABLE (UserID, Location)
 - TABLE (UserID, Country)

The XML data has been analysed with the following regular expression:

"<Bidder [a-zA-Z0-9\"@=.\s]*>[\s]*<[Location|Country]+>[\sa-zA-Z,]*<[/Location|Country]+>[\s]*</Bidder>"

to check that the 'Location' and 'Country' fields can be omitted independently of each other, i.e. there are records with 'Location', but no 'Country' and vice versa, as well as both being skipped. This justifies the split of 'Location' and 'Country' into separate tables.

The temporarily used field for easy to follow explanations, 'BidderID', is to be changed with 'UserID', as this is the field that identifies a bidder.

Reflections of the above reasoning and resulting normalisation are shown in Iteration 3

=========================================================== Iteration 3 ================================================================

TABLE Items (
	ItemID,
	Name,
	Currently,
	First_Bid,
	Location 		REFERENCES Locations(Location),
	Country,
	Started,
	Ends,
	SellerID 		REFERENCES Sellers(UserID),		[=UserID, attribute of Seller]
	Description,

	PRIMARY KEY (ItemID)
)

TABLE Item_Categories (
	ItemID			REFERENCES Items(ItemID),
	Category,

	PRIMARY KEY (ItemID, Category)
)

TABLE Item_Buy_Price (
	ItemID
	Buy_Price,

	FOREIGN KEY (ItemID) REFERENCES Items(ItemID)
)

TABLE Item_Bids (
	ItemID			REFERENCES Items(ItemID),
	UserID 			REFERENCES Bidders(UserID),
	Time 			REFERENCES Bids(Time),

	PRIMARY KEY (ItemID, UserID, Time)
)

TABLE Locations (
	Location,
	Latitude,
	Longitude,

	PRIMARY KEY (Latitude, Longitude)
)

TABLE Sellers (
	UserID,
	Rating,

	PRIMARY KEY (UserID)
)

TABLE Bids (
	UserID 			REFERENCES Bidders(UserID),
	Time,
	Amount,

	PRIMARY KEY (UserID, Time)
)

TABLE Bidders (
	UserID,
	Rating,

	PRIMARY KEY (UserID)
)

TABLE Bidder_Location (
	UserID,
	Location

	FOREIGN KEY (UserID) REFERNCES Bidders(UserID)
)

TABLE Bidder_Country (
	UserID,
	Country,

	FOREIGN KEY (UserID) REFERNCES Bidders(UserID)
)
