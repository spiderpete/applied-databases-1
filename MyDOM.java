/* Parser skeleton for processing item-???.xml files. Must be compiled in
 * JDK 1.5 or above.
 *
 * Instructions:
 *
 * This program processes all files passed on the command line (to parse
 * an entire diectory, type "java MyParser myFiles/*.xml" at the shell).
 *
 */

import java.io.*;
import java.text.*;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ErrorHandler;


class MyDOM {
    
	static final String[] tables = {"Items", "ItemCategories", "ItemBuyPrice", "ItemBids",
    		"Locations", "Sellers", "Bids", "Bidders", "BidderLocation", "BidderCountry"};
	
    static final String columnSeparator = "|*|";
    static final String columnEnd = "|¬";
    static final String subDir = "CSVs";
    static final String tableSuffix = "_table";
    static final String csvExtension = ".csv";
    static DocumentBuilder builder;
    
    static final String[] typeName = {
	"none",
	"Element",
	"Attr",
	"Text",
	"CDATA",
	"EntityRef",
	"Entity",
	"ProcInstr",
	"Comment",
	"Document",
	"DocType",
	"DocFragment",
	"Notation",
    };
    
    static class MyErrorHandler implements ErrorHandler {
        
        public void warning(SAXParseException exception)
        throws SAXException {
            fatalError(exception);
        }
        
        public void error(SAXParseException exception)
        throws SAXException {
            fatalError(exception);
        }
        
        public void fatalError(SAXParseException exception)
        throws SAXException {
            exception.printStackTrace();
            System.out.println("There should be no errors " +
                               "in the supplied XML files.");
            System.exit(3);
        }
        
    }
    
    /* Returns the amount (in XXXXX.xx format) denoted by a money-string
     * like $3,453.23. Returns the input if the input is an empty string.
     */
    static String strip(String money) {
        if (money.equals(""))
            return money;
        else {
            double am = 0.0;
            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
            try { am = nf.parse(money).doubleValue(); }
            catch (ParseException e) {
                System.out.println("This method should work for all " +
                                   "money values you find in our data.");
                System.exit(20);
            }
            nf.setGroupingUsed(false);
            return nf.format(am).substring(1);
        }
    }
    
    // Conversion of date and time format
    static String datatimeFormatter(String oldDateAndTime) {
        if (oldDateAndTime.equals(""))
            return oldDateAndTime;
        else {
        	Date date;
        	String newDateAndTime = oldDateAndTime;
			try {
				date = new SimpleDateFormat("MMM-dd-yyyy hh:mm:ss").parse(oldDateAndTime);
	        	newDateAndTime = new SimpleDateFormat("2yyy-MM-dd hh:mm:ss").format(date);
			} catch (ParseException e) {
				System.out.println("There was a problem converting the timestamp.");
				e.printStackTrace();
			}
        	
        	return newDateAndTime;
        }
    }
    
    /* Process one items-???.xml file.
     */
    static void processFile(File xmlFile, HashSet<String>[] hashTables, FileWriter[] csvFiles) {
        Document doc = null;
        try {
            doc = builder.parse(xmlFile);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(3);
        }
        catch (SAXException e) {
            System.out.println("Parsing error on file " + xmlFile);
            System.out.println("  (not supposed to happen with supplied XML files)");
            e.printStackTrace();
            System.exit(3);
        }
        
        /* At this point 'doc' contains a DOM representation of an 'Items' XML
         * file. Use doc.getDocumentElement() to get the root Element. */
        System.out.println("Successfully parsed - " + xmlFile);
        
        //recursiveDescent(doc, 0);
        
        /* Fill in code here (you will probably need to write auxiliary
            methods). */
        Element nodeItems = doc.getDocumentElement();        
        NodeList items = nodeItems.getChildNodes();
        
        for (int itemElem = 0; itemElem < items.getLength(); itemElem++) {
        	// Tuple arrays for all CSV tables
        	String[] recordItems 			= new String[10];
            String[] recordItemCategories 	= new String[2];
            String[] recordItemBuyPrice		= new String[2];
            String[] recordItemBids 		= new String[3];
            String[] recordLocations 		= new String[3];
            String[] recordSellers 			= new String[2];
            String[] recordBids 			= new String[3];
            String[] recordBidders 			= new String[2];
            String[] recordBidderLocation	= new String[2];
            String[] recordBidderCountry	= new String[2];
            
        	// if node is an element
    		Node item = items.item(itemElem);
        	if (item.getNodeType() == 1) {
        		NodeList itemFields = item.getChildNodes();
        		
        		// Extract ItemID's value
        		NamedNodeMap itemAttribs = item.getAttributes();
        		Node itemID = itemAttribs.getNamedItem("ItemID");
        		
        		if (itemID.getNodeName().equals("ItemID")) {
        			String itemIDValue = itemID.getNodeValue();
        			
        			recordItems[0] 			= itemIDValue;
		    		recordItemCategories[0] = itemIDValue;
		    		recordItemBuyPrice[0] 	= itemIDValue;
		    		recordItemBids[0] 		= itemIDValue;
        		}
        		
        		boolean optionalLongLatPresent = false;
        		for (int itemField = 0; itemField < itemFields.getLength(); itemField++) {
        			Node field = itemFields.item(itemField);
        			
        			if (field.getNodeType() == 1) {
        				String strField = field.getNodeName();
        				Node nodeFirstChild = field.getFirstChild();

        				// The values of the elements are store in their text children
        				// ATTENTION: fieldValue is NOT the Node's value, but rather it's text child value
        				String fieldValue = null;
        				if (nodeFirstChild != null && nodeFirstChild.getNodeType() == 3) {
        					fieldValue = nodeFirstChild.getNodeValue();
        				}
        				
        				switch (strField) {
	    			    	case "ItemID":	    			    		
	    			    		//recordItems[0] 			= fieldValue;
	    			    		//recordItemCategories[0] = fieldValue;
	    			    		//recordItemBuyPrice[0] 	= fieldValue;
	    			    		//recordItemBids[0] 		= fieldValue;
	    			    		System.out.println("Never Ever expected to fall here!!!!!");
	    			    		break;
	    				    case "Name":
	    				    	
	    				    	recordItems[1] 			= fieldValue;
	    				    	break;
	    				    case "Category":
	    				    	Node categoryNode 		= field.getFirstChild();
	    				    	String categoryValue 	= categoryNode.getNodeValue();
	    				    	recordItemCategories[1]	= categoryValue;
	    				    	// Save Item-Category tuple into ItemCategories Table
	    	        			saveRecordToCSV(recordItemCategories, hashTables[1], csvFiles[1]);
	    				    	break;
	    				    case "Currently":
	    				    	recordItems[2]			= strip(fieldValue);
	    				    	break;
	    				    case "Buy_Price":
	    				    	recordItemBuyPrice[1] 	= strip(fieldValue);
	    				    	// Save Item-BuyPrice tuple into ItemBuyPrice Table
	    	        			saveRecordToCSV(recordItemBuyPrice, hashTables[2], csvFiles[2]);
	    				    	break;
	    				    case "First_Bid":
	    				    	recordItems[3]			= strip(fieldValue);
	    				    	break;
	    				    case "Bids":
	    				    	NodeList bids			= field.getChildNodes();
	    				    	
	    				    	for (int bidElem = 0; bidElem < bids.getLength(); bidElem++) {
    				    			Node bid = bids.item(bidElem);
    				    			
    				    			// If a Bid:
	    				    		if (bid.getNodeType() == 1) {
	    				    			NodeList bidFields = bid.getChildNodes();
	    				    			
	    				    			for (int bidFieldElem = 0; bidFieldElem < bidFields.getLength(); bidFieldElem++) {
	    				    				Node bidField = bidFields.item(bidFieldElem);
	    				    				
	    				    				// Only consider the Element nodes of Bids
	    				    				if (bidField.getNodeType() == 1) {
	    				    					boolean optionalLocationPresent 	= false;
	    				    					boolean  optionalCountryPresent 	= false;
	    				    					String strBidField 					= bidField.getNodeName();
	    				        				
	    				        				switch (strBidField) {
	    					    			    	case "Bidder":
	    					    			    		NamedNodeMap bidderAttribs 	= bidField.getAttributes();
	    					    			    		NodeList bidderFields 		= bidField.getChildNodes();
	    					    			    		
	    					    			    		for (int bidderAttribField = 0; bidderAttribField < 2; bidderAttribField++) {
	    					    			    			Node bidderAttrib 			= bidderAttribs.item(bidderAttribField);
	    					    			    			String strbidderAttrib 		= bidderAttrib.getNodeName();
	    					    			    			String bidderAttribValue 	= bidderAttrib.getNodeValue();
	    					    			    			
	    					    			    			switch (strbidderAttrib) {
						    			    			    	case "Rating":
						    			    			    		recordBidders[1] 		= bidderAttribValue;
						    			    			    		break;
						    			    				    case "UserID":
						    			    				    	recordItemBids[1]		= bidderAttribValue;
						    			    				    	recordBids[0]			= bidderAttribValue;
						    			    				    	recordBidders[0]		= bidderAttribValue;
						    			    				    	recordBidderLocation[0]	= bidderAttribValue;
						    			    				    	recordBidderCountry[0]	= bidderAttribValue;
						    			    				    	break;
	    					    			    			}
	    					    			    		}
	    					    			    		
	    					    			    		for (int bidderElem = 0; bidderElem < bidderFields.getLength(); bidderElem++) {
	    					    			    			Node bidderField = bidderFields.item(bidderElem);
	    					    			    			
	    					    			    			if (bidderField.getNodeType() == 1) {
	    					    			    				String strBidderField			= bidderField.getNodeName();
	    					    			    				Node bidderFirstChild 			= bidderField.getFirstChild();
	    					    			    				String bidderFieldValue			= bidderFirstChild.getNodeValue();
	    					    		        				
	    					    		        				switch (strBidderField) {
	    					    			    				    case "Location":
	    					    			    				    	recordBidderLocation[1]	= bidderFieldValue;
	    					    			    				    	optionalLocationPresent	= true;
	    					    			    				    	break;
	    					    			    				    case "Country":
	    					    			    				    	recordBidderCountry[1]	= bidderFieldValue;
	    					    			    				    	optionalCountryPresent	= true;
	    					    			    				    	break;
	    					    		        				}
	    					    			    			}
	    					    			    		}
	    					    			    		
	    					    			    		// Save Bidder tuple into Bidders Table
	    					    		            	saveRecordToCSV(recordBidders, hashTables[7], csvFiles[7]);
	    					    		            	if (optionalLocationPresent) {
		    					    		            	// Save Bidder-Location tuple into BidderLocation Table
		    					    		            	saveRecordToCSV(recordBidderLocation, hashTables[8], csvFiles[8]);
	    					    		            	}
	    					    		            	if (optionalCountryPresent) {
		    					    		            	// Save Bidder-Country tuple into BidderCountry Table
		    					    		            	saveRecordToCSV(recordBidderCountry, hashTables[9], csvFiles[9]);
	    					    		            	}
	    					    			    		
	    					    			    		break;
	    					    				    case "Time":
	    					    				    	Node timeNode 		= bidField.getFirstChild();
	    					    				    	String timeValue 	= timeNode.getNodeValue();
	    					    				    	recordItemBids[2]	= datatimeFormatter(timeValue);
	    					    				    	recordBids[1]		= datatimeFormatter(timeValue);
	    					    				    	break;
	    					    				    case "Amount":	    				    	
	    					    				    	Node amountNode 	= bidField.getFirstChild();
	    					    				    	String amountValue 	= amountNode.getNodeValue();
	    					    				    	recordBids[2]		= strip(amountValue);
	    					    				    	break;
	    				        				}
	    				    				}
	    				    			}
	    				    			
	    				    			// Save Item-User-Time/Bid tuple into ItemBids Table
			    		            	saveRecordToCSV(recordItemBids, hashTables[3], csvFiles[3]);
			    		            	// Save User-Time-Amount tuple into Bids Table
			    		            	saveRecordToCSV(recordBids, hashTables[6], csvFiles[6]);
	    				    		}
	    				    	}
	    				    	break;
	    				    case "Location":
	    				    	Node itemLocationNode					= field.getFirstChild();
	    				    	NamedNodeMap itemlocationAttribs		= field.getAttributes();
	    				    	
	    				    	if (itemlocationAttribs.getLength() != 0) {
	    				    		for (int itemLocAttrib = 0; itemLocAttrib < 2; itemLocAttrib++) {
	    				    			Node locationAttribNode 		= itemlocationAttribs.item(itemLocAttrib);
    			        				String strItemLocationField 	= locationAttribNode.getNodeName();
    			        				String itemLocationFieldValue 	= locationAttribNode.getNodeValue();
    			        				
		    				    		switch(strItemLocationField) {
					    				    case "Latitude":
					    				    	recordLocations[1]		= itemLocationFieldValue;
					    				    	optionalLongLatPresent	= true;
					    				    	break;
					    				    case "Longitude":
					    				    	recordLocations[2] 		= itemLocationFieldValue;
					    				    	optionalLongLatPresent	= true;
					    				    	break;
		    				    		}
	    				    		}
	    				    	}
	    				    	String itemLocationValue 	= itemLocationNode.getNodeValue();
	    				    	recordLocations[0]			= itemLocationValue;
	    				    	recordItems[4]				= itemLocationValue;
	    				    	break;
	    				    
	    				    case "Country":
	    				    	Node itemCountryNode 		= field.getFirstChild();
	    				    	String inteCountryValue		= itemCountryNode.getNodeValue();
	    				    	recordItems[5] 				= inteCountryValue;
	    				    	break;
	    				    case "Started":
	    				    	Node startedNode 			= field.getFirstChild();
	    				    	String startedValue			= startedNode.getNodeValue();
	    				    	recordItems[6]				= datatimeFormatter(startedValue);
	    				    	break;
	    				    case "Ends":
	    				    	Node endsNode 				= field.getFirstChild();
	    				    	String endsValue			= endsNode.getNodeValue();
	    				    	recordItems[7]				= datatimeFormatter(endsValue);
	    				    	break;
	    				    case "Seller":
	    				    	NamedNodeMap sellerAttribs	= field.getAttributes();
	    				    	
	    				    	for (int sellerAttrib = 0; sellerAttrib < 2; sellerAttrib++) {
	    				    		Node sellerAttribNode 	= sellerAttribs.item(sellerAttrib);
	    				    		String sellerAttribName = sellerAttribNode.getNodeName();
	    				    		String sellerFieldValue = sellerAttribNode.getNodeValue();
	    				    		
		    				    	switch(sellerAttribName) {
				    				    case "UserID":
				    				    	recordItems[8] 	= sellerFieldValue;
				    				    	recordSellers[0]= sellerFieldValue;
				    				    	break;
				    				    case "Rating":
				    				    	recordSellers[1]= sellerFieldValue;
				    				    	break;
	    				    		}
	    				    	}
	    				    	break;
	    				    case "Description":
	    				    	String descriptionValue		= fieldValue;
	    				    	recordItems[9]				= descriptionValue;
	    				    	break;
	    				    default:
	    				    	break;
	    				}
        			}
        		}
        		// Save Item tuple into Items Table
				saveRecordToCSV(recordItems, hashTables[0], csvFiles[0]);
				// Save Sellers tuple into Sellers Table
				saveRecordToCSV(recordSellers, hashTables[5], csvFiles[5]);
				if (optionalLongLatPresent) {
					// Save Location tuple into Locations Table
					saveRecordToCSV(recordLocations, hashTables[4], csvFiles[4]);
				}
        	}
        }
        
        /**************************************************************/
        
    }
    
    public static void recursiveDescent(Node n, int level) {
        // adjust indentation according to level
        for(int i=0; i<4*level; i++)
            System.out.print(" ");
        
        // dump out node name, type, and value  
        String ntype = typeName[n.getNodeType()];
        String nname = n.getNodeName();
        String nvalue = n.getNodeValue();
        
        System.out.println("Type = " + ntype + ", Name = " + nname + ", Value = " + nvalue);
        
        // dump out attributes if any
        org.w3c.dom.NamedNodeMap nattrib = n.getAttributes();
        if(nattrib != null && nattrib.getLength() > 0)
            for(int i=0; i<nattrib.getLength(); i++)
                recursiveDescent(nattrib.item(i),  level+1);
        
        // now walk through its children list
        org.w3c.dom.NodeList nlist = n.getChildNodes();
        
        for(int i=0; i<nlist.getLength(); i++)
            recursiveDescent(nlist.item(i), level+1);
    }  
    
    private static void saveRecordToCSV(String[] record, HashSet<String> hashTable, FileWriter csvFile) {
    	StringBuilder tuple = new StringBuilder();
    	for (int i = 0; i < record.length; i++) {
    		String column = record[i];
    		tuple.append(column);
    		if (i < record.length - 1) {
    			tuple.append(columnSeparator);
    		}
    		
    	}
    	// End of Column
    	tuple.append(columnEnd);
		tuple.append("\n");
		
		boolean recordExists = hashTable.contains(tuple.toString());
		if (!recordExists) {
	    	try {
	    		hashTable.add(tuple.toString());
				csvFile.write(tuple.toString());
				csvFile.flush();
			} catch (IOException e) {
				System.out.println("There was a problem writing to the file.");
				e.printStackTrace();
			}
		}
    }
    
    private static void closeCSVFiles(FileWriter[] files) {
    	for (int i = 0; i < files.length; i++) {
    		try {
				files[i].close();
			} catch (IOException e) {
				System.out.println("There was a problem trying to close file writer: " + tables[i]);
				e.printStackTrace();
			}
    	}
    }
    
    @SuppressWarnings("unused")
	private static void arrayToPrint(String[] array) {
    	for (int i = 0; i < array.length; i++) {
    		System.out.println("\t" + array[i]);
    	}
    	System.out.println();
    }
    
    public static void main (String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: java MyParser [file] [file] ...");
            System.exit(1);
        }
        
        /* Initialize parser. */
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            factory.setIgnoringElementContentWhitespace(true);      
            builder = factory.newDocumentBuilder();
            builder.setErrorHandler(new MyErrorHandler());
        }
        catch (FactoryConfigurationError e) {
            System.out.println("unable to get a document builder factory");
            System.exit(2);
        } 
        catch (ParserConfigurationException e) {
            System.out.println("parser was unable to be configured");
            System.exit(2);
        }
        
        // Creation of Table CSV files
        new File("." + File.separator + subDir).mkdir();
        
        File[] files = new File[tables.length];
        for (int i = 0; i < files.length; i++) {
        	File file = new File(subDir + File.separator + tables[i] + tableSuffix + csvExtension);
        	try {
				file.createNewFile();
				files[i] = file;
			} catch (IOException e) {
				System.out.println(subDir + File.separator + tables[i] + tableSuffix + csvExtension);
				System.out.println("There was a problem trying to create file: " + tables[i]);
				e.printStackTrace();
			}
        }
        FileWriter[] fileWriters = new FileWriter[tables.length];
        for (int i = 0; i < fileWriters.length; i++) {
        	try {
				@SuppressWarnings("resource")
				FileWriter fileWriter = new FileWriter(files[i]);
				fileWriters[i] = fileWriter;
			} catch (IOException e) {
				System.out.println("There was a problem trying to create a file writer: " + tables[i]);
				e.printStackTrace();
			}
        }
        
        @SuppressWarnings("unchecked")
		HashSet<String>[] hashTables = new HashSet[tables.length];
        for (int i = 0; i < hashTables.length; i++) {
        	HashSet<String> hashTable = new HashSet<String>();
        	hashTables[i] = hashTable;
        }
        
        /* Process all files listed on command line. */
        for (int i = 0; i < args.length; i++) {
            File currentFile = new File(args[i]);
            processFile(currentFile, hashTables, fileWriters);
        }
        
        closeCSVFiles(fileWriters);
    }
}
