CREATE DATABASE IF NOT EXISTS ad;
USE ad;

CREATE TABLE IF NOT EXISTS Items(
	ItemID 		int NOT NULL, 
	Name 		varchar(100), 
	Currently 	decimal(8,2), 
	First_Bid 	decimal(8,2),
	Location 	varchar(100)	REFERENCES Locations(Location),
	Country 	varchar(100), 
	Started 	datetime, 
	Ends 		datetime, 
	SellerID	int				REFERENCES Sellers(UserID), 
	Description varchar(4000), 

	PRIMARY KEY (ItemID)
);

CREATE TABLE IF NOT EXISTS Item_Categories(
	ItemID 		int NOT NULL	REFERENCES Items(ItemID), 
	Category 	varchar(100), 

	PRIMARY KEY (ItemID, Category)
);

CREATE TABLE IF NOT EXISTS Item_Buy_Price(
	ItemID 		int NOT NULL, 
	Buy_Price 	decimal(8,2), 

	FOREIGN KEY (ItemID)		REFERENCES Items(ItemID)
);

CREATE TABLE IF NOT EXISTS Item_Bids(
	ItemID 		int NOT NULL, 
	UserID 		int, 
	Bid_Time 	datetime, 

	PRIMARY KEY (ItemID, UserID, Bid_Time)
);

CREATE TABLE IF NOT EXISTS Locations(
	Location 	varchar(100) NOT NULL,
	Lattitude 	float(8,6), 
	Longtitude 	float(8,6),

	PRIMARY KEY (Location)
);

CREATE TABLE IF NOT EXISTS Sellers(
	UserID 		int NOT NULL, 
	Rating 		int,

	PRIMARY KEY(UserID)
);

CREATE TABLE IF NOT EXISTS Bids(
	UserID 		int NOT NULL	REFERENCES Bidders(UserID), 
	Bid_Time 	datetime, 
	Amount 		decimal(8,2), 

	PRIMARY KEY (UserID, Bid_Time)
);

CREATE TABLE IF NOT EXISTS Bidders(
	UserID 		int NOT NULL, 
	Rating 		int, 

	PRIMARY KEY (UserID)
);

CREATE TABLE IF NOT EXISTS Bidder_Location(
	UserID 		int NOT NULL, 
	Location 	varchar(100), 

	FOREIGN KEY (UserID)		REFERENCES Bidders(UserID)
);

CREATE TABLE IF NOT EXISTS Bidder_Country(
	UserID 		int NOT NULL, 
	Country 	varchar(100), 

	FOREIGN KEY (UserID)		REFERENCES Bidders(UserID)
);
