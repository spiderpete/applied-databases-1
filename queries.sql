USE ad;
SELECT COUNT(UserID) FROM (SELECT Bidders.UserID FROM Bidders UNION SELECT Sellers.UserID FROM Sellers) user;
SELECT COUNT(ItemID) FROM Items WHERE Items.Location = "New York";
SELECT COUNT(ItemID) FROM Items WHERE ItemID in (SELECT ItemID FROM Item_Categories GROUP BY ItemID HAVING COUNT(ItemID) = 4);---
SELECT ItemID FROM Items WHERE ItemID in (SELECT MAX(Currently) FROM Items WHERE Currently in(SELECT ItemID FROM Items WHERE Items.Ends > '2001-12-20 00:00:01'));---
SELECT COUNT(Sellers.UserID) FROM Sellers WHERE Sellers.Rating > 1000;
SELECT COUNT(UserID) FROM (SELECT Bidders.UserID FROM Bidders, Sellers WHERE Bidders.UserID = Sellers.UserID) user;
SELECT COUNT(Distinct Category) FROM Item_Bids, Bids, Item_Categories WHERE Item_Categories.ItemID = Item_Bids.ItemID AND Bids.UserID = Item_Bids.UserID AND Bids.Bid_Time = Item_Bids.Bid_Time AND Bids.Amount > 100;
